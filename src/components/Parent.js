import { Component } from "react";
import Display1 from "./Display1";
import Display2 from "./Display2";

class Parent extends Component{
    constructor(props){
        super(props);
        this.state={
            display:true
        }
    }
    updateDisplay1 = () => {
        this.setState({
            display: false
        })
    }
    updateDisplay2 = () => {
        this.setState({
            display: true  
        })
    }

    render(){
        return(
            <div className="col-sm-12 text-center">
                {this.state.display?<Display1 display={this.updateDisplay1}/> : <Display2 display={this.updateDisplay2}/>}
            </div>
        )
    }
}
export default Parent;