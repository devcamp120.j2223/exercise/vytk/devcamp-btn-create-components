import { Component } from "react";

class Display1 extends Component{
    componentWillMount() {
        console.log('Component Will Mount');
    }

    componentDidMount() {
        console.log('Component Did Mount');
    }
    componentWillUnmount() {
        console.log("Component will unmount");
    }
    render(){
        return(
            <div>
                <button className="btn btn-info text-light mt-5" onClick={this.props.display}>Create Component</button>
            </div>
        )
    }
}

export default Display1;