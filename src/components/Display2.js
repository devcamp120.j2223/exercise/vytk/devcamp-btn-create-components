import { Component } from "react";

class Display2 extends Component{
    render(){
        return(
            <div>
                <button className="btn btn-danger text-light mt-5" onClick={this.props.display}>Create Component</button>
                <h1>I exist !</h1>
            </div>
        )
    }
}

export default Display2